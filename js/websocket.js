var ws = new WebSocket("ws://echo.websocket.org", "myProtocol");
ws.onopen = function() {
console.log('WebSocket opened');
doSend('Hello');
}
ws.onmessage = function(msg) {
 console.log('Received from server: '+ msg.data);
}
ws.onclose = function() {
console.log('WebSocket closed');
}
function doSend(msg){
 if (ws) {
 ws.send(msg);
 console.log('Sent to server: ' +msg);
 }
}
